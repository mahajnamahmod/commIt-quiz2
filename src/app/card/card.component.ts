import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Observable } from "rxjs/Observable";

@Component({
  selector: "app-card",
  templateUrl: "./card.component.html",
  styleUrls: ["./card.component.css"]
})
export class CardComponent implements OnInit {
  @Input() card;
  @Output() flipEmitter = new EventEmitter();
  constructor() {}

  flipCard() {
    console.log("flipping " + this.card);
    console.log(this.card.flipped);
    if (!this.card.flipped) {
      this.card.flipped = true;
      this.flipEmitter.emit(this.card);
    }
  }
  ngOnInit() {
    console.log("the card");
    console.log(this.card);
    if (this.card.cardObservable) {
      this.card.cardObservable.subscribe(message => {
        this.card.flipped = false;
        console.log('not flipped :)');
      });
    }
  }
}
