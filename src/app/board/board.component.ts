import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
@Component({
  selector: "app-board",
  templateUrl: "./board.component.html",
  styleUrls: ["./board.component.css"]
})
export class BoardComponent implements OnInit {
  private boardSize: number;
  private cardArr = [];
  private wins = 0;
  private clicks: number = 0;
  private prevCard = undefined;
  private view = 'choose';
  constructor() {
  }
  reset(){
    this.cardArr = [];
    this.wins = 0;
    this.clicks = 0;
    this.prevCard = undefined;
    this.view= 'choose';
  }
  chooseDef(difficulty){
    this.boardSize = difficulty
    this.cardArr = [];
    for (let i = 0; i < difficulty; i++) {
      this.cardArr.push(
        {
          flipped: false,
          data: i,
          cardObservable: new BehaviorSubject("def")
        },
        {
          flipped: false,
          data: i,
          cardObservable: new BehaviorSubject("def")
        }
      );
    }
    this.shuffleArray(this.cardArr);
    this.view = 'play';
  }

  handleFlippedCard(cardFlipped){
    if((this.clicks % 2) == 0 ){
      this.prevCard = cardFlipped;
    }else{
      if(!(this.prevCard.data === cardFlipped.data)){
        let timeoutId = setTimeout(() => {  
          this.prevCard.cardObservable.next("true");
          cardFlipped.cardObservable.next("true");
        }, 500);
        
      }else{
        this.wins++;
        console.log(this.wins);
      }
    }
    this.clicks++;
  }

  ngOnInit() {
    
  }

  /**
   * shuffle answers and quetions
   * Using Durstenfeld shuffle algorithm.
   */
  public shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor((i + 1) * Math.random());
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }
}
